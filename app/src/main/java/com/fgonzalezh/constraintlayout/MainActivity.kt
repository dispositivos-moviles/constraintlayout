package com.fgonzalezh.constraintlayout

import android.animation.Animator
import android.animation.ValueAnimator
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.transition.TransitionManager
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mars = findViewById<ImageView>(R.id.marte)

        val animator = ValueAnimator.ofInt(0, 359)
        animator.addUpdateListener { value ->
            val animatedValue = value.animatedValue as Int

            val layoutParams = mars.layoutParams as ConstraintLayout.LayoutParams
            layoutParams.circleAngle = animatedValue.toFloat()

            mars.layoutParams = layoutParams
        }

        animator.repeatMode = ValueAnimator.RESTART
        animator.repeatCount = ValueAnimator.INFINITE
        animator.interpolator = LinearInterpolator()
        animator.duration = 5000

        animator.start()
        //AnimatorSet
    }
}